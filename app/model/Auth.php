<?php 
class Auth
{
   private $table = "users";
   private $db;

   public function __construct()
   {
      $this->db = new Database;
   }

   // register user | create user
   public function createUser($post){
      if ($post["password"] == $post["passwordVerify"]) {
         $query = "INSERT INTO {$this->table} (username, email, first_name, last_name, password) VALUES (:username, :email, :first_name, :last_name, :password)";
         $this->db->query($query);

         $this->db->bind('username', $post['username']);
         $this->db->bind('email', $post['email']);
         $this->db->bind('first_name', $post['firstName']);
         $this->db->bind('last_name', $post['lastName']);
         $this->db->bind('password', md5($post['password']) + SALT);

         $this->db->execute();

         return $this->db->rowCount();
      }
      else{
         return 0;
      }

   }

   public function login($post){
      $this->db->query("SELECT * FROM {$this->table} WHERE email=:email");

      $this->db->bind("email",$post["email"]);

      $data = $this->db->resultSingle();

      $verifyPass = $data["password"];

      if (md5($post['password']) + SALT == $verifyPass) {
         $_SESSION["login"] = "sudah login";
         $_SESSION["email"] = $data["email"];
         return "berhasil";
      }
      else {
         return "gagal";
      }
   }
}
