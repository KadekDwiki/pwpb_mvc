<?php
class User_model
{
   private $table = "users";
   private $db;

   private $name = "kadek dwiki";
   public function getUser(){
      return $this->name;
   }

   public function __construct()
   {
      $this->db = new Database;
   }

   public function getAllUser(){
      $this->db->query("SELECT * FROM {$this->table}");
      return $this->db->resultAll();
   }

   public function getUserById($id){
      $this->db->query("SELECT * FROM {$this->table} WHERE id=:id");
      $this->db->bind("id", $id);

      return $this->db->resultSingle();
   }

   // menampilkan data user yang sudah login
   public function showProfile($post){
      $query = "SELECT * FROM {$this->table} WHERE email=:email";
      $this->db->query($query);

      $this->db->bind("email", $post["email"]);

      return $this->db->resultSingle();

   }

   public function editData($post){
      $query = "UPDATE {$this->table} SET username=:username, email=:email, first_name=:firstname, last_name=:lastname WHERE id=:id";
      $this->db->query($query);

      $this->db->bind("id", $post["id"]);
      $this->db->bind("username", $post["username"]);
      $this->db->bind("email", $post["email"]);
      $this->db->bind("firstname", $post["firstName"]);
      $this->db->bind("lastname", $post["lastName"]);
      // $this->db->bind("password", md5($post["password"]) + SALT);

      $this->db->execute();

      return $this->db->rowCount();

   }

      public function delete($id){
      $query = "DELETE FROM {$this->table} WHERE id=:id";
      $this->db->query($query);

      $this->db->bind("id", $id);

      $this->db->execute();

      return $this->db->rowCount();
   }

}
