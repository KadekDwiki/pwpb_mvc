<?php 
class Login extends Controller{
   public function index(){
      $data['title'] = "Login";
      $this->view("templates/header", $data);
      $this->view("user/login", $data);
      $this->view("templates/footer", $data);

   }

   public function checkLogin(){
      if ($this->model('Auth')->login($_POST) == "berhasil") {
         header('Location:' . BASE_URL . '/home');
         exit;
      }else {
         header('Location:' . BASE_URL . '/login');
         exit;
      }
   }
}