<?php
class Register extends Controller
{
      public function index($company = "SKENSA"){
      $data['title'] = "Register";
      $data['company'] = $company;
      $this->view('templates/header', $data);
      $this->view('user/register', $data);
      $this->view('templates/footer');
   }

   public function insert(){
      
      if ($this->model('Auth')->createUser($_POST) > 0) {
         Flasher::setFlash('Berhasil', "user baru sudah berhasil ditambahkan", "success");
         header('Location:' . BASE_URL . '/login');
         exit;
      }else {
         Flasher::setFlash('Gagal', "menambahkan user baru gagal", "danger");
         header('Location:' . BASE_URL . '/register');
         exit;
      }
   }

}
