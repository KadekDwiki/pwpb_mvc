<?php

class User extends Controller
{
   public function __construct(){
      if (!isset($_SESSION["login"])) {
         header("Location:" . BASE_URL . "/login");
         exit;
      }
   }
   public function index(){
      $data["title"] = "profil user";
      $data["user"] = $this->model("User_model")->showProfile($_SESSION);
      $data['company'] = $data["user"]["username"];

      $this->view("templates/header", $data);
      $this->view("templates/navbar", $data);
      $this->view("user/profile", $data);
      $this->view("templates/footer");
   }

   public function edit($id){
      $data["id"] = $id;
      $data["title"] = "update user";
      $data["user"] = $this->model("User_model")->getUserById($id);
      $data['company'] = $data["user"]["username"];

      $this->view("templates/header", $data);
      $this->view("templates/navbar", $data);
      $this->view("user/edit", $data);
      $this->view("templates/footer");
   }

   public function update(){
      if ($this->model("User_model")->editData($_POST) > 0) {
         Flasher::setFlash('Berhasil', "data user sudah berhasil diupdate", "success");
         header("Location:" . BASE_URL . "/user/profile");
         exit;
      }else {
         Flasher::setFlash('Gagal', "gagal mengupdate data", "danger");
         header("Location:" . BASE_URL . "/user/edit");
         exit;
      }

   }

   public function delete($id){
      if ($this->model("User_model")->delete($id) > 0) {
         header("Location:" . BASE_URL . "/home/users");
         exit;
      }else {
         header("Location:" . BASE_URL . "/home/users");
         exit;
      }
   }

   public function logout(){
      session_destroy();
      return header("Location:" . BASE_URL . "/login");
      exit;
   }
}
