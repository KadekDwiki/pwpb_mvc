<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>MVC | <?= $data['title']; ?></title>
   <link rel="canonical" href="https://getbootstrap.com/docs/5.2/examples/dashboard/dashboard.css">
   <link href="<?= BASE_URL ?>/assets/css/bootstrap.min.css" rel="stylesheet">
   <link href="<? BASE_URL ?>/assets/css/dashboard.css" rel="stylesheet">
   <link rel="stylesheet" href="https://getbootstrap.com/docs/5.2/examples/dashboard/dashboard.css">
   <link href="<? BASE_URL ?>/assets/css/style.css" rel="stylesheet">
</head>
<body>