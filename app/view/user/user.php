<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
   <h6><?= $_SESSION["login"] ?></h6>
   <h6><?= $_SESSION["email"] ?></h6>
   <h2>section, <?= !isset($data["company"]) ?: $data["company"] ?> </h2>
   <a href="<?= BASE_URL ?>/register" class="btn btn-primary">add user</a>
   <h2>daftar user</h2>
   <div class="col-lg-6">
      <?php Flasher::flash(); ?>
   </div>
      <div class="table-responsive">
         <table class="table table-striped table-sm">
            <thead>
            <tr>
               <th scope="col">ID</th>
               <th scope="col">USER NAME</th>
               <th scope="col">EMAIL</th>
               <th scope="col">FIRST NAME</th>
               <th scope="col">LAST NAME</th>
               <th scope="col">ACTION</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data['users'] as $i => $data ) { 
            ?>
            <tr>
               <td><?= ++$i?></td>
               <td><?= $data['username'] ?></td>
               <td><?= $data['email'] ?></td>
               <td><?= $data['first_name'] ?></td>
               <td><?= $data['last_name'] ?></td>
               <td>
                  <a href="<?= BASE_URL ?>/user/edit/<?=$data['id']?>" class="btn btn-warning">edit</a>
                  <a href="<?= BASE_URL ?>/user/delete/<?=$data['id']?>" class="btn btn-danger">hapus</a>
               </td>
            </tr>
            <?php }?>
            </tbody>
         </table>
      </div>
</main>