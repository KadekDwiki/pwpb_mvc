<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4 h-screen">
   <h2 class="my-4">Selamat datang kembali, <?= !isset($data["company"]) ?: $data["company"] ?> </h2>
   <a href="<?= BASE_URL ?>/user/edit/<?= $data["user"]["id"] ?>" class="btn btn-primary">Update data</a>
      <div class="col-lg-6 mt-3">
      <?php Flasher::flash(); ?>
   </div>
   <div class="table-responsive mt-3">
      <table class="table table-striped table-sm">
         <thead>
            <tr>
               <th scope="col">Data user</th>
            </tr>
         </thead>
         <tbody>
         <tr>
            <td>Username </td>
            <td><?= $data["user"]["username"] ?></td>
         </tr>
         <tr>
            <td>Email </td>
            <td><?= $data["user"]["email"] ?></td>
         </tr>
         <tr>
            <td>First Name </td>
            <td><?= $data["user"]["first_name"] ?></td>
         </tr>
         <tr>
            <td>Last Name </td>
            <td><?= $data["user"]["last_name"] ?></td>
         </tr>
         </tbody>
      </table>
   </div>
</main>