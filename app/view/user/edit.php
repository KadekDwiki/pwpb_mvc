<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
   <h3 class="mt-3">Edit Data</h3>
   <form method="POST" action="<?= BASE_URL ?>/user/update" class="w-100 p-4">
      <input type="hidden" name="id" value="<?= $data["user"]["id"] ?>">
         <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Email address</label>
            <input type="email" name="email" class="form-control" id="exampleInputEmail1" value="<?= $data["user"]["email"] ?>">
         </div>
         <div class="mb-3">
            <label for="username" class="form-label">Username</label>
            <input type="text" name="username" class="form-control" id="username" value="<?= $data["user"]["username"] ?>">
         </div>
         <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">First Name</label>
            <input type="text" name="firstName" class="form-control" id="exampleInputEmail1" value="<?= $data["user"]["first_name"] ?>">
         </div>
         <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Last Name</label>
            <input type="text" name="lastName" class="form-control" id="exampleInputEmail1" value="<?= $data["user"]["last_name"] ?>">
         </div>
         <!-- <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Password</label>
            <input type="password" name="password" class="form-control" id="exampleInputPassword1" value="<?= $data["user"]["password"] ?>">
         </div> -->
      <button type="submit" name="submit" class="btn btn-lg btn-primary" >Submit</button>
   </form>
</main>